class DrawCard < BaseService

  def initialize(game_player, params)
    @game_player = game_player
    @params = params
  end

  def result
    @steps
  end

  def call
    ActiveRecord::Base.transaction do
      add_game_step_for_player!
      update_if_dealer_skips!
      update_if_busted!

      self
    end
  end

  private
  def add_game_step_for_player!
    @steps = []
    @params[:cards].each do |param|
      card = Card.new(param[:value], param[:type])
      step = @game_player.steps.new
      step.card = card
      @steps << step

      step.save!
    end
  end

  def update_if_dealer_skips!
    @game_player.skipped! if @game_player.has_dealer_reached_skip_limit?
  end

  def update_if_busted!
    @game_player.busted! if @game_player.has_been_busted?
  end

end
