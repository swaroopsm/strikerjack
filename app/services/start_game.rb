class StartGame < BaseService

  def initialize(dealer, player)
    @dealer = dealer
    @player = player
  end

  def result
    @game
  end

  def call
    ActiveRecord::Base.transaction do
      create_game!
      create_game_players!

      self
    end
  end

  private
  def create_game!
    @game = Game.create!(status: :started)
  end

  def create_game_players!
    create_dealer_player!
    create_normal_player!
  end

  def create_dealer_player!
    game_player = GamePlayer.new
    game_player.game = @game
    game_player.player = @dealer

    game_player.save!
  end

  def create_normal_player!
    game_player = GamePlayer.new
    game_player.game = @game
    game_player.player = @player

    game_player.save!
  end
end
