sj.ui.Winner = React.createClass({

  render: function() {
    var player = this.props.player;

    return (
      <div className='cards-list'>
        <h3>{ player.get('name') } wins!</h3>
        <a className='btn btn-lg btn-primary' href='/games'>View Statistics</a>
      </div>
    );
  }
});
