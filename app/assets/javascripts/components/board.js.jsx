sj.ui.Board = React.createClass({

  renderCards: function(player) {
    var cards = player.get('cards');

    return cards.map(function(card, index) {
      return (
        <div className='col-md-2'>
          <sj.ui.Card card={ card } key={ index } />
        </div>
      )
    });
  },

  render: function() {
    var player = this.props.player;

    return (
      <div className='cards-list'>
        <div className="col-md-12">
          <h3>{ player.get('name') }: </h3>
          { this.renderCards(player) }
        </div>
      </div>
    );
  }
});
