sj.ui.StartScreen = React.createClass({

  render: function() {
    if(this.props.started) { return null; }

    return (
      <a className='btn btn-lg btn-primary'
         onClick={ this.props.onStart }>Start</a>
    );
  }
});
