sj.ui.Actions = React.createClass({

  render: function() {
    var disabled = this.props.disabled;

    return (
      <div className='col-md-12 actions'>
        <div className='col-md-4'>
          <a onClick={ this.props.onHit } disabled={ disabled } className='btn btn-lg btn-success'>HIT</a>
        </div>

        <div className='col-md-4'>
          <a onClick={ this.props.onSkip } disabled={ disabled } className='btn btn-lg btn-danger'>SKIP</a>
        </div>
      </div>
    )
  }
});
