sj.ui.Card = React.createClass({

  render: function() {
    var card = this.props.card;

    return (
      <div className='card well'>
        <h3>{ card.get('value') }</h3>
        <hr />
        <span>{ card.get('type') }</span>
      </div>
    )
  }
});
