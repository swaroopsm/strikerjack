(function() {

  var TOTAL_DECKS = 6;

  sj.ui.Playground = React.createClass({

    _decks: null,

    _player: null,

    _dealer: null,

    getPlayers: function() {
      return [ this._player, this._dealer ];
    },

    reRender: function(callback) {
      if(callback) { callback.call(this); }

      this.forceUpdate();
    },

    getPlayer: function(key, type) {
      var player;

      if(this[key] === null) {
        player = this[key] = new sj.store[type + 'Model'](this._decks);
        player.drawInitialCards(function(toDraw) {
          var cards = this._decks.drawCards(toDraw);

          player.set('cards', cards);
          player.init(function() {
            var winner = this.determineWinner(player);
            if(winner) {
              this.props.onGameOver(winner);
            }
          }.bind(this));
        }.bind(this));
      }

      return this[key];
    },

    determineWinner: function(player) {
      if(player.hasWon()) {
        return player;
      }

      if(player.hasLost()) {
        return _.first(_.reject(this.getPlayers(), function(_player) {
          return _player === player;
        }));
      }

      return false;
    },

    areDecksInitialized: function() {
      return this._decks !== null;
    },

    componentWillMount: function() {
      if(this.props.started && !this.areDecksInitialized()) {
        this._decks = new sj.store.DeckCollection(TOTAL_DECKS);
      }
    },

    handleHit: function(player) {
      var card = this._decks.drawCard();

      player.drawCard(card, function() {
        var winner = this.determineWinner(player);

        if(winner) {
          return this.props.onGameOver(winner);
        }

        this.forceUpdate();
      }.bind(this))
    },

    dealCardForDealer: function() {
      var dealer = this._dealer,
          card = this._decks.drawCard();

      dealer.drawCard(card, function() {
        var winner = this.determineWinner(dealer);

        if(winner) {
          return this.props.onGameOver(winner);
        }

        this.forceUpdate();

        // Deal the card again
        this.dealCardForDealer();
      }.bind(this));
    },

    handleSkip: function(player) {
      player.freeze(function() {
        this.reRender(function() {
          this.dealCardForDealer();
        }.bind(this));
      }.bind(this));
    },

    render: function() {
      var player,
          dealer;

      if(!this.props.started) { return null; }

      player = this.getPlayer('_player', 'NormalPlayer');
      dealer = this.getPlayer('_dealer', 'Dealer');

      return (
        <div>
          <sj.ui.Board player={ player } />
          <sj.ui.Board player={ dealer } />

          <sj.ui.Actions onHit={ this.handleHit.bind(null, player) }
                         onSkip={ this.handleSkip.bind(null, player) }
                         disabled={ !player.canDrawCard() }/>
        </div>
      );
    }
  });
}())
