sj.ui.Game = React.createClass({

  getInitialState: function() {
    return {
      started: false,
      winner: null
    }
  },

  startGame: function() {
    var game;

    game = new sj.store.Game();
    game.create()
        .done(function() {
          this.setState({ started: true, winner: null });
        }.bind(this));
  },

  handleGameOver: function(winner) {
    this.setState({ winner: winner, started: false });
  },

  renderPlayGround: function() {
    if(this.state.winner) { return; }
    if(this.state.started) {
      return <sj.ui.Playground started={ this.state.started }
                               onGameOver={ this.handleGameOver } />
    }
  },

  renderWinnerPage: function() {
    if(this.state.winner) {
      return <sj.ui.Winner player={ this.state.winner } />
    }
    
  },

  renderStartScreen: function() {
    if(!this.state.started) {
      return <sj.ui.StartScreen started={ this.state.started }
                                onStart={ this.startGame } />
    }
  },

  render: function() {
    return (
      <div>
        { this.renderStartScreen() }
        { this.renderPlayGround() }
        { this.renderWinnerPage() }
      </div>
    );
  }
});
