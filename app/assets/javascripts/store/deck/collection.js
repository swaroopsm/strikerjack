(function() {

  function DeckCollection(n) {
    var deck,
        cards = [];

    this._decks = [];
    this._deckCards;

    for(var i=0; i<n; i++) {
      deck = new sj.store.DeckModel();
      cards = cards.concat(deck.getCards());

      this._decks.push(deck);
    }

    this._deckCards = _.shuffle(cards);
  }

  DeckCollection.prototype.getDecks = function() {
    return this._decks;
  };

  DeckCollection.prototype.getCards = function() {
    return this._deckCards;
  };

  DeckCollection.prototype.drawCard = function() {
    return this.getCards().pop();
  };

  DeckCollection.prototype.drawCards = function(n) {
    var cards = [];

    for(var i=0; i<n; i++) {
      cards.push(this.drawCard());
    }

    return cards;
  };

  sj.store.DeckCollection = DeckCollection;
}());
