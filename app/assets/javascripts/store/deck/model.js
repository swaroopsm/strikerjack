(function() {

  var CARD_TYPES,
      SPECIAL_CARDS;

  CARD_TYPES = [ 'clubs', 'diamonds', 'hearts', 'spades' ];
  SPECIAL_CARDS = [ 'A', 'K', 'Q', 'J' ]; // Includes face cards + Ace

  function Deck() {
    var _cards = [];

    // Contains the list of all cards
    this._cards;

    _.each(CARD_TYPES, function(cardType) {
      // Generate normal cards
      for(var i=2, length=10; i<=length; i++) {
        _cards.push(generateCard(cardType, i));
      }

      // Generate special cards
      _.each(SPECIAL_CARDS, function(val) {
        _cards.push(generateCard(cardType, val));
      });
    });

    this._cards = _.shuffle(_cards);
  }

  Deck.prototype.getCards = function() {
    return this._cards;
  };

  // Private Methods
  var generateCard = function(type, value) {
    return new sj.store.CardModel(type, value);
  }

  sj.store.DeckModel = Deck;
}());
