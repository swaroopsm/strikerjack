//= require '../model'
(function() {

  var START_CARDS = 1;

  function DealerModel() {
    sj.store.Model.call(this);

    this.set('name', 'Dealer');
    this.set('type', 'Dealer');
    this._startCards = START_CARDS;
  }

  DealerModel.prototype = Object.create(sj.store.PlayerModel.prototype);

  DealerModel.prototype.canDrawCard = function() {
    return true;
  };

  DealerModel.prototype.keepDrawing = function(card, callback) {
    var fn = this.drawCard(card);

    fn.done(function(data) {
      if(data.success && data.status === null) {
        var card = callback(true);

      }
    }.bind(this));
  };

  sj.store.DealerModel = DealerModel;
}());
