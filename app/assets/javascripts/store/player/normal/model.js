(function() {

  var START_CARDS = 2;

  function NormalPlayerModel(callback) {
    sj.store.Model.call(this);

    this._freeze = false;
    this.set('name', 'Player');
    this.set('type', 'NormalPlayer');
    this._startCards = START_CARDS;
  }

  NormalPlayerModel.prototype = Object.create(sj.store.PlayerModel.prototype);

  NormalPlayerModel.prototype.canDrawCard = function() {
    return this._freeze === false;
  };

  NormalPlayerModel.prototype.freeze = function(callback) {
    this.sendRequest('/games/skip', 'POST', this.attributes)
        .done(function(data) {
          if(data.success) {
            this._freeze = true;
            callback();
          }
        }.bind(this));
  };

  sj.store.NormalPlayerModel = NormalPlayerModel;
}());
