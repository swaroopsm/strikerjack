(function() {

  function PlayerModel() {
    sj.store.Model.call(this);
  }

  var cardDrawnCallback = function(data, callback) {
    if(data.success) {
      this.set('status', data.status)
          .set('won', data.won);

      callback();
    }
  }

  PlayerModel.prototype = Object.create(sj.store.Model.prototype);

  PlayerModel.prototype.hasCards = function() {
    return this.cards > 0;
  };

  PlayerModel.prototype.drawInitialCards = function(callback) {
    callback(this._startCards);
  };

  PlayerModel.prototype.drawCard = function(card, callback) {
    var attributes = _.clone(this.attributes);

    if(this.canDrawCard()) {
      this.add('cards', card);

      // HACK
      attributes.cards = [ card.attributes ];

      this.sendRequest('/games/draw', 'POST', attributes)
          .done(function(data) {
            cardDrawnCallback.call(this, data, callback);
          }.bind(this));
    }

    return;
  };

  PlayerModel.prototype.init = function(callback) {
    var attributes = _.clone(this.attributes);

    // HACK
    attributes.cards = attributes.cards.map(function(card) {
      return card.attributes;
    });

    this.sendRequest('/games/draw', 'POST', attributes)
        .done(function(data) {
          cardDrawnCallback.call(this, data, callback);
        }.bind(this));
  };

  PlayerModel.prototype.hasWon = function() {
    var status = this.get('won');

    return status === true;
  };

  PlayerModel.prototype.hasLost = function(callback) {
    var status = this.get('won');

    return status === false;
  };

  sj.store.PlayerModel = PlayerModel;
}());
