(function() {

  function Game() {
    this.url = '/games';
  };

  Game.prototype = Object.create(sj.store.Model.prototype);

  sj.store.Game = Game;
}());
