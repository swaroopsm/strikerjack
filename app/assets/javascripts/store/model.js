(function() {

  function Model() {
    this.attributes = {};
  }
  
  var makeRequest = function(url, type, data) {
    var req;

    req = $.ajax({
      type: type,
      data: JSON.stringify(data),
      url: url,
      contentType: 'application/json',
      dataType: 'json'
    });

    return req;
  };

  Model.prototype.sendRequest = makeRequest;

  Model.prototype.create = function() {
    var req = makeRequest(this.url, 'POST', this.attributes);

    return req;
  };

  Model.prototype.fetch = function() {

  };

  Model.prototype.set = function(key, val) {
    this.attributes[key] = val;

    return this;
  };

  Model.prototype.add = function(key, val) {
    var attr = this.attributes[key];
    if(attr === undefined) { attr = []; }
    if(_.isArray(attr)) { attr.push(val); }

    return this;
  };

  Model.prototype.get = function(key) {
    return this.attributes[key];
  };

  sj.store.Model = Model;
}());
