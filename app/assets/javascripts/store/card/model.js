(function() {

  function CardModel(type, value) {
    sj.store.Model.call(this);

    this.set('type', type)
        .set('value', value);
  }

  CardModel.prototype = Object.create(sj.store.Model.prototype);

  sj.store.CardModel = CardModel;
}());
