class GamesController < ApplicationController

  before_action :game, only: :show

  def index
    @games = Game.order(id: :asc)
                 .includes(game_players: [ :player ])
  end

  def show

  end

  def create
    player = NormalPlayer.first
    dealer = Dealer.first

    outcome = StartGame.call(dealer, player)
    result = outcome.result
    session[:game_id] = result.id

    render json: result
  end

  def draw
    DrawCard.call(game_player, draw_params)

    render json: {
      success: true,
      status: game_player.status,
      won: game_player.won?
    }
  end

  def skip
    if current_player.is_a?(NormalPlayer) && !game_player.busted?
        game_player.skipped!
        json = { success: true }
    else
      json = { success: false }
    end

    render json: json
  end

  private
  def current_player
    @current_player ||= params[:type].classify.constantize.first
  end

  def draw_params
    params.permit(cards: [ :type, :value ])
  end

  def game_player
    current_game.game_players.find_by!(player_id: current_player)
  end

  def game
    @game ||= Game.find(params[:id])
  end

  def current_game
    Game.find(session[:game_id])
  end
end
