module GameHelper

  def game_winner(players)
    players.each do |player|
       yield(player) and return if player.won?
    end
  end
end
