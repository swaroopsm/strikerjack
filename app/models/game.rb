class Game < ActiveRecord::Base

  MAX_POINTS = 21

  enum status: [ :started, :finished, :aborted ]

  # Associations
  has_many :game_players
  has_many :players, through: :game_players
  has_many :steps, through: :game_players
end
