class Card

  attr_reader :value, :type

  FACE_CARDS = [ 'K', 'Q', 'J' ]
  ACE_CARD = 'A'
  TYPES = {
    clubs: 1,
    diamonds: 2,
    hearts: 3,
    spades: 4
  }

  # TODO
  # Use a config object
  POINTS = {
    face_card: 10,
    ace_card: 11
  }

  def initialize(value, type)
    @value = value
    @type = TYPES[type.to_sym]
  end

  def face_card?
    FACE_CARDS.include?(@value)
  end

  def ace_card?
    @value === ACE_CARD
  end

  def points
    return POINTS[:face_card] if face_card?
    return POINTS[:ace_card] if ace_card?

    # If normal card
    return @value
  end
end
