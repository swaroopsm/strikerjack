class GamePlayer < ActiveRecord::Base

  enum status: [ :skipped, :busted ]

  DEALER_SKIP_LIMIT = 16

  # Associations
  has_many   :steps, class_name: GamePlayerStep
  belongs_to :game
  belongs_to :player

  # Validations
  validates :game, presence: true
  validates :player, presence: true
  
  def has_been_busted?
    points > Game::MAX_POINTS
  end

  def points
    steps.sum(:points)
  end

  def won?
    return false if busted?

    points === Game::MAX_POINTS ||
    opponent.busted? ||
    has_more_points? ||
    nil
  end

  def has_more_points?
    skipped? && opponent.skipped? && self.points >= opponent.points
  end

  # TODO
  # Support multi players
  # For now it will be only 1
  def opponent
    GamePlayer.where.not(player: self.player)
              .where(game: game)
              .first
  end

  def dealer?
    player.is_a? Dealer
  end

  def has_dealer_reached_skip_limit?
    dealer? && points > DEALER_SKIP_LIMIT
  end

  def normal_player?
    player.is_a? NormalPlayer
  end
end
