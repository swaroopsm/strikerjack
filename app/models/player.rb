class Player < ActiveRecord::Base

  # Associations
  has_many :game_players
  has_many :games, through: :game_players
end
