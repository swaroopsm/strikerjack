class GamePlayerStep < ActiveRecord::Base

  # Associations
  belongs_to :game_player

  # Validations
  validates :card_type, presence: true
  validates :card_value, presence: true
  validates :points, presence: true

  def card=(card)
    @card = card
    self.card_value = card.try(:value)
    self.card_type = card.try(:type)
  end

  def card
    @card ||= Card.new(card_value, card_type)
  end

  before_validation :set_points

  private
  def set_points
    self.points = card.points if card && new_record?
  end
end
