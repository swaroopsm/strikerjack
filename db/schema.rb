# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160302135105) do

  create_table "game_player_steps", force: :cascade do |t|
    t.integer  "game_player_id"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.integer  "card_type"
    t.string   "card_value"
    t.integer  "points"
  end

  add_index "game_player_steps", ["game_player_id"], name: "index_game_player_steps_on_game_player_id"

  create_table "game_players", force: :cascade do |t|
    t.integer  "game_id"
    t.integer  "player_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "status"
  end

  add_index "game_players", ["game_id"], name: "index_game_players_on_game_id"
  add_index "game_players", ["player_id"], name: "index_game_players_on_player_id"
  add_index "game_players", ["status"], name: "index_game_players_on_status"

  create_table "games", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "status"
  end

  create_table "players", force: :cascade do |t|
    t.string   "name"
    t.string   "type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "players", ["type"], name: "index_players_on_type"

end
