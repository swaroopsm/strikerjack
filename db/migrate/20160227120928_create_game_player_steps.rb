class CreateGamePlayerSteps < ActiveRecord::Migration
  def change
    create_table :game_player_steps do |t|
      t.references :game_player, index: true, foreign_key: true
      t.string :card

      t.timestamps null: false
    end

    add_index :game_player_steps, :card
  end
end
