class AddStatusToGamePlayers < ActiveRecord::Migration
  def change
    add_column :game_players, :status, :integer
    add_index  :game_players, :status
  end
end
