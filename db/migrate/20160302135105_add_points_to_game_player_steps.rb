class AddPointsToGamePlayerSteps < ActiveRecord::Migration
  def change
    add_column :game_player_steps, :points, :integer
  end
end
