class AddCardTypeAndCardValueToGamePlayerSteps < ActiveRecord::Migration
  def change
    add_column :game_player_steps, :card_type, :integer
    add_column :game_player_steps, :card_value, :string
  end
end
