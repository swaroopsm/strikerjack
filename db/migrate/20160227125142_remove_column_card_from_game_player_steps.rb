class RemoveColumnCardFromGamePlayerSteps < ActiveRecord::Migration
  def change
    remove_column :game_player_steps, :card
  end
end
