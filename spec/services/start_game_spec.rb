require 'rails_helper'

describe StartGame do

  let(:dealer) { create(:dealer) }
  let(:normal_player) { create(:normal_player) }

  context 'when params are correct' do

    before do
      outcome = StartGame.call(dealer, normal_player)
      @game = outcome.result
    end

    it 'creates a game' do
      expect(Game.count).to eq(1)
    end

    it 'creates the correct players for the game' do
      expect(@game.players.count).to eq(2)
    end

    it 'creates game player who is a dealer' do
      expect(@game.players).to include(dealer)
    end

    it 'creates game player who is a normal player' do
      expect(@game.players).to include(normal_player)
    end
  end
end
