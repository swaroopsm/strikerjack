require 'rails_helper'

RSpec.describe GamePlayerStep, type: :model do

  describe 'Associations' do
    it { is_expected.to belong_to(:game_player) }
  end

  describe '#card' do

    subject { build(:game_player_step) }

    it 'is an instance of `Card`' do
      expect(subject.card).to be_instance_of(Card)
    end
  end

  describe '#card_type' do
    context 'when card is not initialized' do
      subject { build(:game_player_step, card: nil) }

      it 'has the correct value for `card_type`' do
        expect(subject.card_type).to be_nil
      end
    end

    context 'when card is initialized' do
      subject { build(:game_player_step) }

      it 'has the correct value for `card_type`' do
        expect(subject.card_type).to eq(subject.card.type)
      end
    end
  end

  describe '#card=' do
    context 'value is nil' do
      subject { build(:game_player_step) }
      
      before do
        subject.card = nil
      end

      it 'set nil value for card type' do
        expect(subject.card.type).to be_nil
      end

      it 'set nil value for card value' do
        expect(subject.card.value).to be_nil
      end
    end

    context 'value is a Card object' do
      let(:card) { build(:card) }
      subject { build(:game_player_step) }
      
      before do
        subject.card = card
      end

      it 'has the correct value for card type' do
        expect(subject.card.type).to eq(card.type)
      end

      it 'has the correct value for card value' do
        expect(subject.card.value).to eq(card.value)
      end
    end
  end

  describe '#card_value' do
    context 'when card is not initialized' do
      subject { build(:game_player_step, card: nil) }

      it 'has the correct value for `card_value`' do
        expect(subject.card_value).to be_nil
      end
    end

    context 'when card is initialized' do
      subject { build(:game_player_step) }

      it 'has the correct value for `card_value`' do
        expect(subject.card_value).to eq(subject.card.value)
      end
    end
  end
end
