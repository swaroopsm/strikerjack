require 'rails_helper'

RSpec.describe Player, type: :model do

  describe 'Associations' do
    it { is_expected.to have_many(:game_players) }
    it { is_expected.to have_many(:games) }
  end
end
