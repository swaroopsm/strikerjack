require 'rails_helper'

RSpec.describe Card, type: :model do

  describe 'TYPES' do
    
    it 'has the correct values' do
      expected = {
        clubs: 1,
        diamonds: 2,
        hearts: 3,
        spades: 4
      }

      expect(Card::TYPES).to eq(expected)
    end
  end

  describe '#initialize' do
    context 'when args are correct' do
      let(:value) { 'K' }
      let(:type) { Card::TYPES[:hearts] }
      subject { Card.new('K', Card::TYPES[:hearts]) }

      it 'sets type' do
        expect(subject.type).to eq(type)
      end

      it 'sets value' do
        expect(subject.value).to eq(value)
      end
    end
  end
end
