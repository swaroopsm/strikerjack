require 'rails_helper'

RSpec.describe GamePlayer, type: :model do

  describe 'Associations' do
    it { is_expected.to belong_to(:game) }
    it { is_expected.to belong_to(:player) }
  end

  describe 'Validations' do
    it { is_expected.to validate_presence_of(:game) }
    it { is_expected.to validate_presence_of(:player) }
  end
end
