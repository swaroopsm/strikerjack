FactoryGirl.define do
  factory :game_player_step do
    card { build(:card) }
    game_player
  end
end
