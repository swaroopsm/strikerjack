FactoryGirl.define do
  factory :player do
    name Faker::Name.name

    factory :dealer do
      type { Dealer }
    end

    factory :normal_player do
      type { NormalPlayer }
    end
  end
end
