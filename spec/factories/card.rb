FactoryGirl.define do
  factory :card do
    initialize_with do
      value = (2...10).to_a.sample.to_s
      type = Card::TYPES.values.sample

      new(value, type)
    end
  end
end
